#include <new_home_arduino.hpp>
#include <ESP8266WiFi.h>

class DeviceAction : public NewHomeMethod {
  public:
    bool can_handle(String method_name) {
      return method_name.equals("device_action");
    }

    JSONVar handle_request(JSONVar request) {
      String channel = (const char *) request["arguments"]["channel"];
      JSONVar response;
      response["code"] = 0;

      String method = channel.substring(0, 4);
      channel = channel.substring(7);
      int index = channel.indexOf('/');
      int pin = atoi(channel.substring(0, index).c_str());
      String action = channel.substring(index + 1);

      if (!method.equals("gpio")) {
        response["code"] = 1;
        response["message"]["error"] = "Unknown channel type provided";

        return response;
      }

      int state = 0;

      if (action.equals("on")) {
        state = 1;
      }

      if (action.equals("off")) {
        state = 0;
      }

      if (action.equals("toggle")) {
        state = !digitalRead(pin);
      }

      Serial.println(pin);
      Serial.println(state);
      pinMode(pin, OUTPUT);
      digitalWrite(pin, state);

      response["message"]["success"] = String("State is ") + (state == 0 ? "off" : "on");

      return response;
    }
};

WiFiServer server(5445);
NewHomeMethod *methods[] = {new DeviceAction()};
NewHomeArduino application(server, methods, 1);

void setup() {
  application.setup();
}

void loop() {
  application.loop();
}
