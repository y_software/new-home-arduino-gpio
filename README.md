# New Home GPIO (Arduino)

## Usage

This application is tested with a NodeMCU (+ESP-12F chip) and a plain, tiny ESP-01 module.
Connect it in the same way as any application; by adding it as an application in the new-home-ui and using it as a device in a room.

You can get the IP of your module through your router (which might be a bit of a hassle) or during the boot process in the serial console.


## Installation

- Open the contained .ino in the Arduino IDE
- Add the [new-home-arduino](https://gitlab.com/y_software/new-home-arduino) library to the project. (Download as a ZIP and import it)
- Upload it to the ESP module
  - Note: You will have to enable the SPIFFS (Tools | Flash Size | *any option with the SPIFFS enabled*)

